import React, { useState,useEffect } from 'react'
import { addCart,deleteCart } from '../redux/cartAction';
import { useSelector,useDispatch } from 'react-redux'

function Cart() {
    const [total,setTotal] = useState(0)
    const state = useSelector((state) => state.handleCart)
    const dispatch = useDispatch();

    const incrementButton = (product) =>{
        dispatch(addCart(product))
    }
    const decrementButton = (product) =>{
        dispatch(deleteCart(product))
    }
    useEffect(() => {
        const getTotalItems = () => {
            const total = state.reduce((total, item) => total + (item.qty * item.price), 0);
            setTotal(total)
        }
        getTotalItems();
    },[state])
  return (
    <div className='cart'>
        <div className="row m-5">
            <div className='col-12'>
                <h1 className='display-6 fw-bolder text-center'>
                    My Cart
                </h1>
                <hr/>
            </div>
        </div>
        {state.length > 0 ?
            state.map((product) => {      
                return( 
                    
                        <div className='row m-5 productItem' key={product.id}>      
                            <div className='col-md-4'> 
                                <img src={product.image} alt={product.title} height="200px" width="180px" />
                            </div>
                            <div className='col-md-4'> 
                                <h3>{product.title}</h3>
                                <p className='lead fw-bold'>
                                    {product.qty} X ${product.price} = $
                                    {product.qty * product.price}
                                </p>
                                <button className='btn btn-outline-dark me-4' onClick={()=>decrementButton(product)}>
                                    <i className='fa fa-minus'></i>
                                </button>
                                <button className='btn btn-outline-dark me-4' onClick={()=>incrementButton(product)}>
                                    <i className='fa fa-plus'></i>
                                </button>
                            </div>
                        </div>
                     
                )
            })
            :
            <div className='row m-5 productItem text-center'>
                <h4>No Products</h4>
            </div>
        }
        {
            state.length != 0 && 
                (<div className='row m-5 fw-bolder productItem'>
                    <div className='col-md-4'>
                        Total : ${total}
                    </div>
                   <div className='col-md-8'>
                        <button className='btn btn-dark'>
                            Place Order
                        </button>
                   </div>
                
                </div>)
        }
    </div>
  )
}

export default Cart
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css'

function Header() {
    const state = useSelector((state) => state.handleCart)
  return (
    <div className='navHeader'>
         <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand display-6 fw-bolder" href="#">E-CART</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink to="/" className="nav-link">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/products" className="nav-link">Products</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/about" className="nav-link">About</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/contact" className="nav-link">Contact</NavLink>
                        </li>
                    </ul>
                    <div className='buttons'>
                        <a href='' className='btn btn-outline-dark'>
                            <i className='fa fa-sign-in me-1'> <span className='a-text'> Login</span></i>
                        </a>
                        <a href='' className='btn btn-outline-dark ms-2'>
                            <i className='fa fa-user-plus me-1'> <span className='a-text'> Register</span></i>
                        </a>
                        <NavLink to="/cart" className='btn btn-outline-dark ms-2'>
                            <i className='fa fa-shopping-cart me-1'> <span className='a-text'> Cart({state.length})</span></i>
                        </NavLink>
                    </div>
                </div>
            </div>
        </nav>
    </div>
  )
}

export default Header;

import React, { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton';
import "react-loading-skeleton/dist/skeleton.css";
import { NavLink } from 'react-router-dom';

function Products() {
    const [data,setData] = useState([])
    const [filter,setFilter] = useState(data)
    const [loading,setLoading] = useState(false)
    const [searchText,setSearchText] = useState('')
    let componentMounted = true;

    useEffect(()=>{
        const getProducts = async () => {
            setLoading(true);
            const response = await fetch("https:fakestoreapi.com/products");
            if(componentMounted){
                setData(await response.clone().json());
                setFilter(await response.json());
                setLoading(false);
            }
            return () => {
                 componentMounted = false
            }
        }
        getProducts();
    },[]);

    useEffect(()=>{
        searchProduct();
    },[searchText]);

    const searchProduct = () =>{
        if(searchText.length != 0){
            const filteredProducts = data.filter((product) => {
                if (
                product.title.toLowerCase().includes(searchText) ||
                product.description.toLowerCase().includes(searchText) ||
                product.category.toLowerCase().includes(searchText)
                ){
                return product;
                }
            });
            setFilter(filteredProducts);

        }
        else{
            setFilter(data);
        }   
    }

    const setProductText = (e) =>{
        setSearchText(e.target.value.toLowerCase());      
    }

    const Loading = () => {
        return(
            <>
                <div className="col-md-3">
                    <Skeleton height={350}/>
                </div>
                <div className="col-md-3">
                    <Skeleton height={350}/>
                </div>
                <div className="col-md-3">
                    <Skeleton height={350}/>
                </div>
                <div className="col-md-3">
                    <Skeleton height={350}/>
                </div>
            </>
        )
    }

    const filterProduct = (cat) => {
        const updatedList = data.filter((x)=>x.category === cat);
        setFilter(updatedList);
   }

    const ShowProducts = () => {
        return(
            (filter.length !=0) ?
            <>
                <div className='buttons d-flex justify-content-center m-4'>
                    <button className='btn btn-outline-dark me-2' onClick={() =>setFilter(data)}>All</button>
                    <button className='btn btn-outline-dark me-2' onClick={() =>filterProduct("men's clothing")}>Men's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() =>filterProduct("women's clothing")}>Women's Clothing</button>
                    <button className='btn btn-outline-dark me-2' onClick={() =>filterProduct("jewelery")}>Jewelery</button>
                    <button className='btn btn-outline-dark me-2' onClick={() =>filterProduct("electronics")}>Electronics</button>
                </div>
                {filter.map((product) => {      
                    return(  
                        <div className='col-md-3 mb-4' key={product.id}>      
                            <div className="card h-100 text-center p-4">
                                <img src={product.image} className="card-img-top" alt={product.title} height="250px"/>
                                <div className="card-body">
                                    <h5 className="card-title">{product.title.substring(0,12)}</h5>
                                    <p className="card-text lead fw-bold">${product.price}</p>
                                    <NavLink to={`/products/${product.id}`} className="btn btn-outline-dark">Buy Now</NavLink>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </>
            :
            <div className='row m-5 productItem text-center'>
                <h4>No Products to show</h4>
            </div>
        )
    }

    return (
        <div>
            <div className="container mt-4">
                <div className="row">
                    <div className='col-md-7'>
                        <h1 className='display-6 fw-bolder'>
                            Latest Products
                        </h1>      
                    </div>             
                    <div className='col-md-5 text-center'>
                        <form className="form-inline my-2 my-lg-0">
                            <div className='col-md-12'>
                                <input className="form-control mr-sm-2" type="search" value={searchText} placeholder="Search" aria-label="Search"  onChange={setProductText}/>
                            </div>
                        </form>
                    </div>
            
                    <hr/>
                </div>
                <div className='row justify-content-center'>
                    {loading ? <Loading/> :<ShowProducts/>}
                </div>
            </div>
        </div>
    )
}

export default Products
import React from 'react'
import Products from './Products'

function Home() {
  return (
    <div className='Hero'>
        <div className="card bg-dark text-white border-0">
            <img src="/assets/shopping.jpg" className="card-img" alt="..." height="650px"/>
        </div>
        <Products/>
    </div>
  )
}

export default Home
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import { BrowserRouter,Routes,Route } from "react-router-dom";
import Products from './components/Products';
import Header from './components/Header';
import Product from './components/Product';
import { Provider } from 'react-redux';
import store from './redux/store';
import Cart from './components/Cart';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Provider store={store}>
      <Header />
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="products">
          <Route index element={<Products/>}/>
          <Route path=":id" element={<Product/>}/>
        </Route>  
        <Route path="cart" element={<Cart/>}/>     
      </Routes>
      </Provider>
  </BrowserRouter>,
);

